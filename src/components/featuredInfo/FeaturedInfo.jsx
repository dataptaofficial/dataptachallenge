import "./featuredInfo.css"
import {ArrowDownward, ArrowUpward} from "@material-ui/icons"

export default function FeaturedInfo() {
  return (
    <div className="featured">
        <div className="featuredItem">
            <span className="featuredTitle">
                Mid Market Price    
            </span>
            <div className="featuredMoneyContainer">
                <span className="featuredMoney">35.347,28</span>
                <span className="featuredMoneyRate">
                +374,30 (1,07%) <ArrowUpward className="featuredIcon"/> 
                today
                </span>
            </div>
            <span className="featuredSub">EUR</span>
        </div>

    </div>
  )
}
