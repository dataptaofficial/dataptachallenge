export const BTCData = [
    {
        name: '0:00',
        "BTC Price": 347,
    },
    {
        name: '2:00',
        "BTC Price": 198,
    },
    {
        name: '4:00',
        "BTC Price": 607,
    },
    {
        name: '6:00',
        "BTC Price": 315,
    },
    {
        name: '8:00',
        "BTC Price": 499,
    },
    {
        name: '10:00',
        "BTC Price": 813,
    },
    {
        name: '12:00',
        "BTC Price": 800,
    },
    {
        name: '14:00',
        "BTC Price": 620,
    },
    {
        name: '16:00',
        "BTC Price": 551,
    },
    {
        name: '18:00',
        "BTC Price": 725,
    },
    {
        name: '20:00',
        "BTC Price": 432,
    },
    {
        name: '22:00',
        "BTC Price": 347,
    },


];