import Chart from "../../components/chart/Chart"
import FeaturedInfo from "../../components/featuredInfo/FeaturedInfo"
import "./home.css"
import { BTCData } from "../../dataService"

export default function Home() {
  return (
    <div className="home">
        <FeaturedInfo/>
        <Chart data={BTCData} title="BTC Analytics (today)" grid dataKey="BTC Price"/>
    </div>
  )
}
